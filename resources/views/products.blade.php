<section id="pricing" class="pricing">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Products</h2>
        <p>We have several types of products. We also offer to build according to your bussiness.</p>
      </div>

      <div class="row">

        <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
          <div class="box">
            <h3>ERP</h3>
            <h4><span>Enterprise Resource Planning</span></h4>
            <ul>
                <li><i class="bx bx-aperture"></i> Sales Force Distribution</li>
                <li><i class="bx bx-collection"></i> Inventory Management</li>
                <li><i class="bx bx-bitcoin"></i> Account Management</li>
                <li><i class="bx bx-alarm"></i> Warehouse Management</li>
            </ul>
          </div>
        </div>

        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
          <div class="box featured">
            <h3>HRM</h3>
            <h4><span>Human Resources Management</span></h4>
            <ul>
                <li><i class="bx bx-user"></i> Customer Relationship Management</li>
                <li><i class="bx bx-archive"></i> School Management</li>
                <li><i class="bx bx-award"></i> House-Rent Management</li>
                <li><i class="bx bx-clinic"></i> Hostipal Management</li>
            </ul>
          </div>
        </div>

        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
          <div class="box">
            <h3>POS</h3>
            <h4><span>Point of Sale</span></h4>
            <ul>
                <li><i class="bx bx-cart"></i> Shop Management</li>
                <li><i class="bx bx-card"></i> Pharmacy Management</li>
            </ul>
          </div>
        </div>

      </div>

    </div>
  </section>
