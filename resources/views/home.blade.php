<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>AnalyticalJ</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <style>
    .tech-logo {
    }
    .btn-contact {
      background: #209dd8;
      border: 0;
      padding: 12px 34px;
      color: #fff;
      transition: 0.4s;
      border-radius: 50px;
    }
    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: #212529;
        vertical-align: middle;
        border-color: #dee2e6;
    }
    .form-contact {
      width: 100%;
      border-top: 3px solid #47b2e4;
      border-bottom: 3px solid #47b2e4;
      padding: 30px;
      background: #fff;
      box-shadow: 0 0 24px 0 rgb(0 0 0 / 12%);
    }
    .contact .form-contact label {
        padding-bottom: 8px;
    }
    .contact .form-contact .form-group {
        margin-bottom: 20px;
    }
    .contact .form-contact input {
        height: 44px;
    }
    .error {
      color: red;
    }
    .error-message {
      color: red;
    }
  </style>
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="{{ route('home')  }}">AnalyticalJ</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          <li><a class="nav-link scrollto" href="#services">Services</a></li>
          <li><a class="nav-link   scrollto" href="#portfolio">Portfolio</a></li>
          <li><a class="nav-link scrollto" href="#team">Team</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="nav-link scrollto" href="#pricing">Products</a></li>
          <li><a class="getstarted scrollto" href="#latest">Latest</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Better Solutions For Your Business</h1>
          <h2>We are team of talented developers building web Application</h2>
          <div class="d-flex justify-content-center justify-content-lg-start">
            <a href="#about" class="btn-get-started scrollto">Get Started</a>
            {{-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a> --}}
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="assets/img/hero-img.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients section-bg">
      <div class="container">

        <div class="row" data-aos="zoom-in">

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-1.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-2.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-3.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-4.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-5.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/client-6.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End Cliens Section -->

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>About Us</h2>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p>
              AnalyticalJ is a software solution company. We offers:
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Promising business solution software</li>
              <li><i class="ri-check-double-line"></i> Regular monitoring & Ascent progress</li>
              <li><i class="ri-check-double-line"></i> Our main focus on client's demand</li>
            </ul>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <p>
                AnalyticalJ is a rising software company in Bangladesh. We are developing world-class quality
                software. We develop intellectual and dynamic software that fully meets the jobs of our clients.
                We are committed to continuous progress in our perception and expertise and thus achieving our success
                through the pursuit of enthusiastic performance and quality. Our main focus on client's demand.
                In fact, it is the long-term relationships with our clients that has served as a solid foundation for our company.
            </p>
            <a href="#" class="btn-learn-more">Learn More</a>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3>Our <strong>Applications</strong></h3>
              <p>
                We develop professional world-class software.
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>01</span> ERP <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                    <p>
                      ERP is an acronym that stands for enterprise resource planning (ERP). It's a business process management software that manages and integrates a company's financials, supply chain, operations, commerce, reporting, manufacturing, and human resource activities.
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>02</span> Account management system <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Accounting management software helps businesses to keep track of all incomes, expenses, and every other financial transaction.
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span> Hospital management system <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                    <p>Hospital management system project helps to store all the kinds of records, provide coordination and user communication, implement policies, improve day-to-day operations, arrange the supply chain, manage financial and human resources, and market hospital services.</p>
                  </div>
                </li>

              </ul>
            </div>

          </div>

          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
            <img src="assets/img/skills.png" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
            <h3>HOW WE WORK</h3>
            <p class="fst-italic">
              We have specialized software team to produce software to assist you to operate your entire company smoothly.
            </p>

            <div class="skills-content">

              <div class="progress">
                <span class="skill">Trustworthy <i class="val">100%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Promising <i class="val">100%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Optimistic <i class="val">100%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Proficiency <i class="val">100%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

            </div>

          </div>
        </div>

      </div>
    </section><!-- End Skills Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Services</h2>
          <p>
            AnalyticalJ is a competent provider of any kind of software solutions including
            mission critical business applications, complex engineering software, data
            centric analytical software, smart device solutions, web solutions and so on.
            Our expertise lies in clearly perceiving the business goals of our customers,
            choosing the most appropriate technologies and efficiently developing solutions
            catered to the business needs
          </p>
        </div>

        <div class="row">
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4><a href="">Software Development</a></h4>
              <p>Enterprise Web Applications</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="">Database Development</a></h4>
              <p>Database Analysis & Reporting</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4><a href="">Software Testing</a></h4>
              <p>Regression & Performance Testing</p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href="">Technology Consultancy</a></h4>
              <p>Application Development Services</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container" data-aos="zoom-in">

        <div class="row">
          <div class="col-lg-9 text-center text-lg-start">
            <h3>Mission & Vision</h3>
            <p>
              We believe in constant innovation and improvements. The Agile methodology emphasizes communication and collaboration. Our team is flexible to adapt to the emerging business realities. It helps us to successfully manage and accomplish the goals of our customers projects worldwide.
            </p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#">Call To Action</a>
          </div>
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    @include('portfolio')
    <!-- End Portfolio Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Team</h2>
          <p>
            Our development team consists of experienced and qualified system analysts,
            application developers, solution developers, database administrators, programmers
            and testers. All of them are certified professionals in their respected
            areas. In each step of development, we use creative ideas, cutting-edge technologies and
            proven methodologies. It is rest assured that the software will be top-rated and most user-friendly.
          </p>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
              <div class="pic"><img src="assets/img/team/team.jpeg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Zahid Al-islam</h4>
                <span>
                    CEO, Software Engineer <br>
                    Mobile: 01708887754 <br>
                    Email: analyticalzahid@gmail.com
                </span>
                <p>Professional committed software engineer.</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href="https://www.facebook.com/zahid.alislam.7/"><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href="https://www.linkedin.com/in/zahid-al-islam-1b1166ab/"> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4 mt-lg-0">
            <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="200">
              <div class="pic"><img src="assets/img/team/team.jpeg" class="img-fluid" alt=""></div>
              <div class="member-info">
                <h4>Jakaria Hossain</h4>
                <span>
                    Business Development Manager <br>
                    Mobile: 01715436121  <br>
                    Email: 	jakaria5468@gmail.com
                </span>
                <p>Talented business development manager.</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= Products Section ======= -->
    @include('products')
    @include('latest')
    <!-- End Products Section -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Technologies</h2>
          <p>We always use latest technologies. We love learning & innovation is our passion.</p>
        </div>

        <div class="row">

          <div class="col-lg-12 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="box">
              <table class="table">
                <tr>
                  <td><img src="assets/img/tech/laravel5.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/mysql.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/angular.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/vue.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/python.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/php.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/js.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/postgres.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/mongodb.png" class="img-fluid tech-logo" alt=""></td>
                  <td><img src="assets/img/tech/nestjs-logo.png" class="img-fluid tech-logo" alt=""></td>
                </tr>
              </table>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Pricing Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Frequently Asked Questions</h2>
        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" class="collapse" data-bs-target="#faq-list-1">How do you ensure quality? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-bs-parent=".faq-list">
                <p>
                  We ensure quality from the very beginning to delivery. We have adopted Test Driven Development,
                  it is a practice in which a test is written first and actual coding is done to pass the test.
                  Our developers write unit test as many as required to maximize the code coverage. We conduct
                  Integration test, regression test and automatic UI test at the end of each iteration. We regularly
                  arrange SQA related training for the development team to increase awareness.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-3" class="collapsed">How do you assure confidentiality? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-bs-parent=".faq-list">
                <p>
                  We do not even add your project name in our portfolio. The software product which we develop
                  for you on basis of "Work made on hire" is entirely yours. We may sign non-disclosure
                  agreement as required. Our employees are bound by company confidentiality and non-disclosure
                  agreement. We have also had our privacy policy defined to protect your privacy.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-4" class="collapsed">Can I expand the system if required in future? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-bs-parent=".faq-list">
                <p>
                  We develop software keeping in mind that a product always evolves. We design the system
                  using separate layers for each concern making a loose coupling. You may want to convert a
                  desktop based application into a web application. As the business logic, data access and infrastructure
                  services remain in separate layers, you will not need a complete rewrite, developing a
                  web UI in the presentation layer will do the work. Same goes about database and so on.
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contact</h2>
          <p>
            If you have any query you can send an email or directly call in any time.
          </p>
        </div>

        <div class="row">

          <div class="col-lg-5 d-flex align-items-stretch">
            <div class="info">
              <div class="address">
                <i class="bi bi-geo-alt"></i>
                <h4>Location:</h4>
                <p>Pallabi, Dhaka, Bangladesh</p>
              </div>

              <div class="email">
                <i class="bi bi-envelope"></i>
                <h4>Email:</h4>
                <p>analyticalzahid@gmail.com</p>
              </div>

              <div class="phone">
                <i class="bi bi-phone"></i>
                <h4>Call:</h4>
                <p>+8801708-887754</p>
              </div>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1824.8761153608014!2d90.36542955872194!3d23.827408099999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c1c856746d1d%3A0x93f85b4d01a1a3bb!2sAnalyticalJ!5e0!3m2!1sen!2sbd!4v1646705643467!5m2!1sen!2sbd" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form role="form" class="form-contact">
              @csrf
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name">Your Name</label>
                  <input type="text" name="name" class="form-control" id="name" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Your Email</label>
                  <input type="email" class="form-control" name="email" id="email" required>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Subject</label>
                <input type="text" class="form-control" name="subject" id="subject" required>
              </div>
              <div class="form-group">
                <label for="name">Message</label>
                <textarea class="form-control" name="message" rows="10" required></textarea>
              </div>
              <div class="my-3">
                {{-- <div class="loading">Loading</div> --}}
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center">
                <button class="btn-contact" type="submit">Send Message</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Join Our Newsletter</h4>
            <p>Enter your email address to subscribe </p>
            <form action="{{ route('subscribe') }}" method="post">
              @csrf
              <input type="email" name="email">
              <input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>AnalyticalJ</h3>
            <p>
              Pallabi, Mirpur-12  <br>
              Dhaka<br>
              Bangladesh <br><br>
              <strong>Phone:</strong> +8801708-887754<br>
              <strong>Email:</strong> analyticalzahid@gmail.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="http://analyticalj.com">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Application</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Ecommerce Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Business Solution</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Industrial Software</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Warehouse Management</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Social Networks</h4>
            <p>You can join with us</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="https://www.facebook.com/Analyticalj" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
              <a href="https://www.linkedin.com/in/zahid-al-islam-1b1166ab/" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>AnalyticalJ</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
        Powered by <a href="https://analyticalj.com/">AnalyticalJ</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

  <script>
    $(function() {
      $('.sent-message').hide();
      if ($(".form-contact").length > 0) {
        $(".form-contact").validate({
          rules: {
            name: {
              required: true,
              maxlength: 50
            },
            email: {
              required: true,
              maxlength: 50,
              email: true,
            },
            message: {
              required: true,
              maxlength: 300
            },
          },
          messages: {
            name: {
              required: "Please enter name",
              maxlength: "Your name maxlength should be 50 characters long."
            },
            email: {
              required: "Please enter valid email",
              email: "Please enter valid email",
              maxlength: "The email name should less than or equal to 50 characters",
            },
            message: {
              required: "Please enter message",
              maxlength: "Your message name maxlength should be 300 characters long."
            },
          },
          submitHandler: function(form) {
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $('.btn-contact').html('Please Wait...');
            $(".btn-contact").attr("disabled", true);
            $.ajax({
              url: "{{url('contact')}}",
              type: "POST",
              data: $('.form-contact').serialize(),
              success: function(data) {
                $('.btn-contact').html('Submit');
                $(".btn-contact").attr("disabled", false);
                if (data.success) {
                  console.log('Data has been submitted successfully');
                  $('.sent-message').show();
                }
              },
              error: function(xhr, textStatus, error){
                let errorMsg = xhr.responseJSON.message
                $('.error-message').html(errorMsg).show()
                $('.btn-contact').html('Submit');
                $(".btn-contact").attr("disabled", false);
              }
            });
          }
        })
      }
    });
  </script>
</body>

</html>
