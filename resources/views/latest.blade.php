<section id="latest" class="pricing">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Some Latest Products</h2>
        <p>Here some live links of our latest products</p>
      </div>

      <div class="row">

        <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
          <div class="box">
            <h3>H-Rent</h3>
            <h4><span>House Rent Management</span></h4>
            <ul>
                <li><i class="bx bx-caret-right"></i> Dashboard Summary</li>
                <li><i class="bx bx-caret-right"></i> Flats Management</li>
                <li><i class="bx bx-caret-right"></i> Renter Management</li>
                <li><i class="bx bx-caret-right"></i> Invoice Management</li>
                <li><i class="bx bx-caret-right"></i> Account Management</li>
            </ul>
            <a href="http://hrent-aj.herokuapp.com" target="_blank" class="buy-btn">Get Started</a>
          </div>
        </div>

        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
          <div class="box featured">
            <h3>Account Management</h3>
            <h4><span>Account Management System</span></h4>
            <ul>
                <li><i class="bx bx-caret-right"></i> Dashboard Summary</li>
                <li><i class="bx bx-caret-right"></i> Customer Management</li>
                <li><i class="bx bx-caret-right"></i> Supplier Management</li>
                <li><i class="bx bx-caret-right"></i> Purchase Management</li>
                <li><i class="bx bx-caret-right"></i> Sale Management</li>
                <li><i class="bx bx-caret-right"></i> Invoice Management</li>
                <li><i class="bx bx-caret-right"></i> Expense Management</li>
                <li><i class="bx bx-caret-right"></i> Account Management</li>
                <li><i class="bx bx-caret-right"></i> Payments Management</li>
                <li><i class="bx bx-caret-right"></i> Reports Management</li>
            </ul>
            <a href="http://analytical-cms.herokuapp.com" class="buy-btn" target="_blank">Get Started</a>
          </div>
        </div>

        <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
          <div class="box">
            <h3>POS</h3>
            <h4><span>Point of Sale</span></h4>
            <ul>
                <li><i class="bx bx-caret-right"></i> Dashboard Summary</li>
                <li><i class="bx bx-caret-right"></i> Cart Management</li>
                <li><i class="bx bx-caret-right"></i> Sale Management</li>
                <li><i class="bx bx-caret-right"></i> Return Management</li>
                <li><i class="bx bx-caret-right"></i> Due Management</li>
                <li><i class="bx bx-caret-right"></i> Purchase Management</li>
                <li><i class="bx bx-caret-right"></i> Purchase Re-order</li>
                <li><i class="bx bx-caret-right"></i> Due Management</li>
                <li><i class="bx bx-caret-right"></i> User Management</li>
                <li><i class="bx bx-caret-right"></i> User Permission</li>
                <li><i class="bx bx-caret-right"></i> Inventory Management</li>
                <li><i class="bx bx-caret-right"></i> Low Stock Notification</li>
                <li><i class="bx bx-caret-right"></i> Balance Sheet</li>
                <li><i class="bx bx-caret-right"></i> Reports Management</li>
            </ul>
          </div>
        </div>

      </div>

    </div>
  </section>
