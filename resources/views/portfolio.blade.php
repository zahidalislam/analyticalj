<section id="portfolio" class="portfolio">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Portfolio</h2>
        <p>Our software team is specialized in producing software to assist you to operate your entire industry smoothly.</p>
      </div>

      <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
        <li data-filter="*" class="filter-active">All</li>
        <li data-filter=".filter-app">HRM</li>
        <li data-filter=".filter-card">Health</li>
        <li data-filter=".filter-web">Account</li>
      </ul>

      <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-img"><img src="assets/img/portfolio/crm.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>CRM</h4>
            <p>HRM</p>
            <a href="assets/img/portfolio/crm.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src="assets/img/portfolio/account.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Account Management</h4>
            <p>Account</p>
            <a href="assets/img/portfolio/account.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-img"><img src="assets/img/portfolio/student3.png" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>School Management</h4>
            <p>HRM</p>
            <a href="assets/img/portfolio/student3.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-img"><img src="assets/img/portfolio/pharmacy.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Pharmacy Management</h4>
            <p>Health</p>
            <a href="assets/img/portfolio/pharmacy.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src="assets/img/portfolio/erp.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>ERP</h4>
            <p>Account</p>
            <a href="assets/img/portfolio/erp.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-img"><img src="assets/img/portfolio/house_rent.png" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>House Rent</h4>
            <p>HRM</p>
            <a href="assets/img/portfolio/house_rent.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-card">
          <div class="portfolio-img"><img src="assets/img/portfolio/hms.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Hospital Management</h4>
            <p>Health</p>
            <a href="assets/img/portfolio/hms.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src="assets/img/portfolio/warehouse.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Warehouse Management</h4>
            <p>Account</p>
            <a href="assets/img/portfolio/warehouse.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 3"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web">
          <div class="portfolio-img"><img src="assets/img/portfolio/inventory.jpg" class="img-fluid" alt=""></div>
          <div class="portfolio-info">
            <h4>Inventory Management</h4>
            <p>Account</p>
            <a href="assets/img/portfolio/inventory.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
            <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
          </div>
        </div>

      </div>

    </div>
  </section>