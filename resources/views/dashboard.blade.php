<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="table">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Message</td>
                                    <td>Date</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contacts as $row)
                                <tr>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->message }}</td>
                                    <td>{{ \Carbon\Carbon::parse($row->created_at)->format('M d, Y') }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<style>
    .table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
    vertical-align: middle;
    border-color: #dee2e6;
}
</style>
