<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Subscribe;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function subscribe(Request $request)
    {
        if ($request->email) {
            $exist = Subscribe::where('email', $request->email)->first();

            if (!$exist) {
                Subscribe::create(['email' => $request->email, 'name' => 'New']);
            }
        }

        return response()->json(['success' => true]);
    }

    public function contact(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|max:255',
            'message' => 'required'
        ]);

        if ($request->email) {
            $subscribe = Subscribe::where('email', $request->email)->first();

            if (!$subscribe) {
                $subscribe = Subscribe::create(['email' => $request->email, 'name' => $request->name]);
            }

            $data = $request->except('_token');

            Contact::create([
                'subscribe_id' => $subscribe->id,
                'name' => $request->name,
                'message' => $request->message,
                'subject' => $request->subject,
            ]);
        }

        return response()->json(['success' => true]);
    }
}
